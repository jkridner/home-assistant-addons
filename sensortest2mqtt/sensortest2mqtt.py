#!/usr/bin/env python3
import socket
import sys
import struct
import time
import re
import paho.mqtt.client as mqtt
import json

sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sock.bind(('', 9999))
sock.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_LOOP, True)
mreq = struct.pack("16s15s".encode('utf-8'), socket.inet_pton(socket.AF_INET6, "ff02::1"), (chr(0) * 16).encode('utf-8'))
sock.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_JOIN_GROUP, mreq)

client = mqtt.Client("sensortest2mqtt.py")
client.connect("localhost")

temperature_re = re.compile('t:(\d+\.\d+);')
humidity_re = re.compile('h:(\d+\.\d+);')
light_re = re.compile('l:(\d+\.\d+);')
rotary_re = re.compile('r:(\d+\.\d+);')

config = {
  "manufacturer": "BeagleBoard.org",
  "model": "BeagleConnect Freedom",
}

def publish(topic, config, value):
  payload = json.dumps(config)
  #print(topic + ":" + payload)
  client.publish(topic, payload)
  topic = config["state_topic"]
  payload = json.dumps(value)
  #print(topic + ":" + payload)
  client.publish(topic, payload)

while True:
  data, sender = sock.recvfrom(1024)
  if str(sender[0])[0:6] == "fe80::":
    bcf_id = str(sender[0])[6:10]
    #print(bcf_id + str(data))
    temperature = temperature_re.search(str(data))
    if temperature:
      config["name"] = "BCF " + bcf_id + " Temperature"
      config["state_topic"] = "homeassistant/sensor/bcf_" + bcf_id + "/state"
      config["value_template"] = "{{ value_json.temperature | default('', true) }}"
      config["device_class"] = "temperature"
      config["unit_of_measure"] = "°C"
      topic = "homeassistant/sensor/bcf_" + bcf_id + "T/config"
      value = { "temperature": temperature[1] }
      publish(topic, config, value)
    humidity = humidity_re.search(str(data))
    if humidity:
      config["name"] = "BCF " + bcf_id + " Humidity"
      config["state_topic"] = "homeassistant/sensor/bcf_" + bcf_id + "/state"
      config["value_template"] = "{{ value_json.humidity | default('', true) }}"
      config["device_class"] = "humidity"
      config["unit_of_measure"] = "%"
      topic = "homeassistant/sensor/bcf_" + bcf_id + "H/config"
      value = { "humidity": humidity[1] }
      publish(topic, config, value)
    light = light_re.search(str(data))
    if light:
      config["name"] = "BCF " + bcf_id + " Light"
      config["state_topic"] = "homeassistant/sensor/bcf_" + bcf_id + "/state"
      config["value_template"] = "{{ value_json.light | default('', true) }}"
      config["device_class"] = "illuminance"
      config["unit_of_measure"] = "lux"
      topic = "homeassistant/sensor/bcf_" + bcf_id + "L/config"
      value = { "light": light[1] }
      publish(topic, config, value)
    rotary = rotary_re.search(str(data))
    if rotary:
      config["name"] = "BCF " + bcf_id + " Rotary"
      config["state_topic"] = "homeassistant/sensor/bcf_" + bcf_id + "/state"
      config["value_template"] = "{{ value_json.rotary | default('', true) }}"
      del config["device_class"]
      del config["unit_of_measure"]
      topic = "homeassistant/sensor/bcf_" + bcf_id + "R/config"
      value = { "rotary": rotary[1] }
      publish(topic, config, value)
